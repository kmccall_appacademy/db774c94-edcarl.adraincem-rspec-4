class Book
  attr_reader :title

  # def title
  #   @title
  # end

  def title=(title)
    split_title = title.split
    exceptions = ["the", "a", "an", "and", "in", "of"]
    cap_title = []

    split_title.each_with_index do |word, idx|
      if idx != 0 && exceptions.include?(word)
        cap_title << word
      else
        cap_title << word.capitalize
      end
    end
    @title = cap_title.join(' ')
  end
end
