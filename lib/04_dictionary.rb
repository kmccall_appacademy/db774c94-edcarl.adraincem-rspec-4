require 'byebug'

class Dictionary
  attr_reader :entries

  def initialize(entries = {})
    @entries = entries
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    else
      entry.each { |keyword, definition| @entries[keyword] = definition }
    end
  end

  def find(entry)
    @entries.select { |keyword| keyword.include?(entry) }
  end

  def printable
    output = keywords.map { |key| "[#{key}] \"#{@entries[key]}\"" }
    output.join("\n")
  end

  def keywords
    @entries.keys.sort
  end

  def include?(entry)
    self.keywords.first == entry
  end
end
