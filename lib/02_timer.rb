class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    sec_int = (seconds % 60)
    min_int = (seconds / 60)
    hr_int = (min_int / 60)
    min_int = min_int % 60 if min_int > 60
    padded(hr_int) + ":" + padded(min_int) + ":" + padded(sec_int)
  end

  def padded(digit)
    digit < 10 ? "0#{digit}" : digit.to_s
  end
end
