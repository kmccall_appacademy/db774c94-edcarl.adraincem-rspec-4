class Temperature

  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def in_fahrenheit
    @fahrenheit.nil? ? self.class.ctof(@celsius) : @fahrenheit
  end

  def in_celsius
    @celsius.nil? ? self.class.ftoc(@fahrenheit) : @celsius
  end

  #factory methods
  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  #utility methods
  def self.ftoc(temp)
    (temp - 32) * 5.0 / 9
  end

  def self.ctof(temp)
    (temp * 9.0 / 5) + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    super(c: temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(f: temp)
  end
end
